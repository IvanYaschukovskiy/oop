#include "TritSet.hpp"
const short _TRITS_IN_UCHAR_ = 4 * sizeof(uchar);
Trits::Trits() : capacity(1), size(1)
{
	tritsMass = new uchar[capacity];
	tritsMass[0] = 0;
}
Trits::Trits(int newSize) : capacity(newSize / _TRITS_IN_UCHAR_ + !!(newSize % _TRITS_IN_UCHAR_)), size(1)
{
	tritsMass = new uchar[capacity];
	for(int i = 0; i < capacity; ++i)
	{
		tritsMass[i] = 0;
	}
}
Trits::Trits(const Trits& set)
{
	copyTrits(set);
}
Trits::Trits(Trits&& set): tritsMass(set.tritsMass), capacity(set.capacity), size(set.size)
{
	set.tritsMass = nullptr;
	set.capacity = 0;
	set.size = 0;
}
Trits::~Trits()
{
	delete[] this->tritsMass;
	tritsMass = nullptr;
}
Trits& Trits::copyTrits(const Trits& right)
{
	this->capacity = right.capacity;
	this->size = right.size;
	this->tritsMass = new uchar[this->capacity];
	for(int i = 0; i < this->capacity; ++i)
	{
		this->tritsMass[i] = right.tritsMass[i];
	}
	return *this;
}
Trits& Trits::copyTritMass(int capacity)
{
	uchar* temp = new uchar[capacity];
	for(int i = 0; i < this->capacity; ++i)
	{
		temp[i] = this->tritsMass[i];
	}
	for (int i = this->capacity; i < capacity; ++i)
	{
	 	temp[i] = 0;
	}
	delete[] this->tritsMass;
	this->tritsMass = temp;
	return *this;
}
void Trits::setTrit(int index, trit value)
{
	if(index > capacity * _TRITS_IN_UCHAR_ - 1)
	{
		if(value == Unknown)
		{
			return;
		}
		int newCapacity = index / _TRITS_IN_UCHAR_ + !!(index % _TRITS_IN_UCHAR_);
		this->copyTritMass(newCapacity);
		this->size = index + 1;
		this->capacity = newCapacity;
	}
	if(index >= size && value != Unknown)
	{
		this->size = index + 1;
	}
	if(value == True)
    {
    	tritsMass[index / _TRITS_IN_UCHAR_] |= (1 << (7 - (index % _TRITS_IN_UCHAR_) * 2));
    }
    else
    {
      	tritsMass[index / _TRITS_IN_UCHAR_] &= ~(1 << (7 - (index % _TRITS_IN_UCHAR_) * 2));
       	if(value == Unknown)
       	{
       		tritsMass[index / _TRITS_IN_UCHAR_] &= ~(1 << (7 - (index % _TRITS_IN_UCHAR_) * 2 - 1));
       		return;
       	}
    }
    tritsMass[index / _TRITS_IN_UCHAR_] |= (1 << (7 - (index % _TRITS_IN_UCHAR_) * 2 - 1));
}
trit Trits::getTrit(int index) const
{
	if (index > size)
	{
		return Unknown;
	}
	uchar tmpValueTrit = static_cast<uchar>(tritsMass[index / _TRITS_IN_UCHAR_] << ((index % _TRITS_IN_UCHAR_) * 2)) >> 6;
	switch(tmpValueTrit)
	{
		case True:
		return True;
		break;
		case False:
		return False;
		break;
		case Unknown:
		return Unknown;
		break;
		default:
		return Unknown;
		break;
	}
}
void Trits::trim(int lastIndex)
{
	if (lastIndex > size)
	{
		return;
	}
	for (int i = lastIndex; i < size; ++i)
	{
		(*this)[i] = Unknown;
	}
	for(int i = lastIndex; i > -1; --i)
	{
		if(this->getTrit(i) != Unknown || i == 0)
		{
			this->size = i + 1;
			break;
		}
	}
}
void Trits::shrink()
{
	uchar* temp;
	for(int i = this->size - 1 ; i > - 1; --i)
	{
		if(this->getTrit(i) != Unknown || i == 0)
		{
			delete[] this->tritsMass;
			if (i > 3)
			{
				this->capacity = i / _TRITS_IN_UCHAR_ + !!(i % _TRITS_IN_UCHAR_);
				temp = new uchar[this->capacity];
			}
			if(i <= 3)
			{
				temp = new uchar[1];
				this->capacity = 1;
			}
			this->size = i + 1;
			break;			
		}
	}
}
int Trits::cardinality(trit value) const
{
	int cardinality = 0;
	for(int i = 0; i < size; ++i)
	{
		if(this->getTrit(i) == value)
		{
			++cardinality;
		}
	}
	return cardinality;
}
int Trits::getLenght() const
{
	return this->size;
}
int Trits::getCapacity() const
{
	return this->capacity;
}
std::unordered_map<trit, int, std::hash<int>> Trits::cardinality() const 
{
	std::unordered_map<trit, int, std::hash<int>> result = { {True, 0}, {False, 0}, {Unknown, 0} };
	for (int i = 0; i < this->size; ++i)
	{
		++result.at(this->getTrit(i));
	}
	return result;
}
Trits& Trits::operator =(const Trits& right)
{
	if (this != &right)
	{
		delete[] this->tritsMass;
		this->copyTrits(right);
    }
    return *this;
}
Trits& Trits::operator =(Trits&& right)
{
	if(this != &right)
	{
		delete[] this->tritsMass;
		this->tritsMass = right.tritsMass;
		this->capacity = right.capacity;
		this->size = right.size;
		right.tritsMass = nullptr;
		right.size = 0;
		right.capacity = 0;
	}
	return *this;
}
Trits& Trits::bigger_smaller(const Trits& right)
{
	if(right.capacity > this->capacity)
	{
		this->copyTritMass(right.capacity);
		this->capacity = right.capacity;
		this->size = right.size;
	}
	return *this;
}
Trits& Trits::operator &=(const Trits& right)
{
	this->bigger_smaller(right);
	for(int i = 0; i < this->size; ++i)
	{
		this->setTrit(i, this->getTrit(i) & right.getTrit(i));
	}
	for(int i = this->size; i != 0; --i)
	{
		if(this->getTrit(i - 1) != Unknown)
		{
			this->size = i;
			break;
		}
	}
	return *this;
}
Trits& Trits::operator |=(const Trits& right)
{
	this->bigger_smaller(right);
	for(int i = 0; i < this->size; ++i)
	{
		this->setTrit(i, this->getTrit(i) | right.getTrit(i));
	}
	for(int i = this->size; i != 0; --i)
	{
		if(this->getTrit(i - 1) != Unknown)
		{
			this->size = i;
			break;
		}
	}
	return *this;
}
Trits Trits::operator ~() const
{
	Trits result = *this;
	for (int i = 0; i < this->size; i++)
	{
		if (this->getTrit(i) == Unknown)
		{
			continue;
		}
		if (this->getTrit(i) == False)
		{
			result.setTrit(i,True);
		}
		else
		{
			result.setTrit(i,False);
		}
	}
	return result;
}
Trits::TritHelp::TritHelp(Trits &s, int i): set(s), index(i){}
Trits::TritHelp::operator trit()
{
	return set.getTrit(index);
}
Trits::TritHelp Trits::TritHelp::operator =(trit value)
{
	set.setTrit(index, value);
	return *this;
}
Trits::TritHelp Trits::operator [](int index)
{
	return Trits::TritHelp(*this, index);
}
trit operator &(trit left, trit right)
{
	if(left == False || right == False)
	{
		return False;
	}
	if(left == True && right == True)
	{
		return True;
	}
	return Unknown;
}
trit operator |(trit left, trit right)
{
	if(left == False && right == False)
	{
		return False;
	}
	if(left == True || right == True)
	{
		return True;
	}
	return Unknown;
}
trit operator ~(trit value)
{
	if(value == False)
	{
		return True;
	}
	if(value == True)
	{
		return False;
	}
	return Unknown;
}
const Trits operator &(const Trits& left, const Trits& right)
{
		Trits result(left);
		return result &= right;
}
const Trits operator |(const Trits& left, const Trits& right)
{
	Trits result(left);
	return result |= right;
}
std::ostream& operator <<(std::ostream& os, trit value)
{
	switch(value)
	{
		case True:
		os << "True";
		break;
		case False:
		os << "False";
		break;
		case Unknown:
		os << "Unknown";
		break;
		default:
		os << "Unknown";
		break;
	}
	return os;
}