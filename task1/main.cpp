#include "TritSet.h"
#include <iostream>
using namespace std;

int main()
{
	uchar i=96;
	cout << (static_cast<int>(static_cast<uchar>(i << 2) >> 6));
	cout << i << "\n";
	i |= (1<<7);
	cout <<static_cast<int>(i) << "\n";
	i &= ~(1<<7);
	cout <<static_cast<int>(i) << "\n";
	Trits a;
	uchar* tritsMass;
	*(a.tritsMass)=192;
	*(a.tritsMass+1)=96;
	cout << static_cast<int>(a.tritsMass[2]);
	return 0;
}
