#ifndef TritSet
#define TritSet
#include <iostream>
#include <unordered_map>
typedef unsigned char uchar;
enum trit 
{
	Unknown = 0, 
	False = 1,
	True = 3
};
trit operator &(trit left, trit right);
trit operator |(trit left, trit right);
trit operator ~(trit value);
class Trits
{
	int capacity;
	int size;
	uchar* tritsMass;
	class TritHelp
	{
		Trits &set;
		int index;
	public:
		TritHelp(Trits &s, int i);
		operator trit();
		TritHelp operator =(trit value);
	};
	Trits& copyTrits(const Trits& right);
	Trits& bigger_smaller(const Trits& right);
	Trits& copyTritMass(int capacity);

public:
	Trits();
	Trits(int newSize);
	Trits(const Trits& set);
	Trits(Trits&& set);
	~Trits();
	void setTrit(int index, trit value);
	trit getTrit(int index) const;
	TritHelp operator [](int index);
	void trim(int lastIndex);
	int cardinality(trit value) const;
	int getLenght() const;
	int getCapacity() const;
	void shrink();
	std::unordered_map<trit, int, std::hash<int>> cardinality() const;
	Trits& operator =(const Trits& right);
	Trits& operator =(Trits&& right);
	Trits& operator &=(const Trits& right);
	Trits& operator |=(const Trits& right);
	Trits operator ~() const;
};
const Trits operator &(const Trits& left, const Trits& right);
const Trits operator |(const Trits& left, const Trits& right);
std::ostream& operator <<(std::ostream& os, trit value);
#endif