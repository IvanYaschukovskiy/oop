#ifndef TritSet
#define TritSet
#define _CRT_SECURE_NO_WARNINGS
typedef unsigned char uchar;
class Trits
{
public:
	int index;
	enum trit {Unknown = 0, False = 1, True = 3};
	uchar* tritsMass;
	char tritPosition;
	//setTrit(){};
	//getTrit(){};
	uchar operator [](int index)
	{
		tritPosition = index % 4;
		return tritsMass[index / 4];
	}
	trit& operator =(const trit& trit)
	{
		if (this == &trit)
		{
            return *this;
        }
        if(trit == True)
        {
        	tritsMass[index] |= (1 << (7 - tritPosition*2));
        	tritsMass[index] |= (1 << (7 - tritPosition*2 - 1));
        }
        else
        {
        	tritsMass[index] &= ~(1 << (7 - tritPosition*2));
        	if(trit == Unknown)
        	{
        		tritsMass[index] &= ~(1 << (7 - tritPosition*2 - 1));
        	}
        }
        return *this;
	}
};
/*setTrit(int size)
{

}
getTrit(int element)
{
	if (element > size)
	{
		return Unknown;
	}
}*/
#endif