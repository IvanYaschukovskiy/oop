import java.lang.StringBuilder;
import java.io.*;
import java.util.*;

class MapAndCount
{
	public Map<String, Integer> hashMap = new HashMap<String, Integer>();
	public int count = 0;
	public void doMap(Reader reader) throws IOException
	{
		StringBuilder stringBuild = new StringBuilder();
		char symbol;
		int symbolInt = reader.read();
		while(symbolInt != -1)
		{
			symbol = (char)symbolInt;
		 	if(Character.isLetterOrDigit(symbol))
		 	{		 		
		 		stringBuild.append(symbol);
		 	}
		 	else
		 	{
		 		if(stringBuild.length() != 0)
		 		{
		 			if(hashMap.containsKey(stringBuild.toString()))
		 			{
		 				hashMap.put(stringBuild.toString(), hashMap.get(stringBuild.toString()) + 1);
		 			}
		 			else
		 			{
		 				hashMap.put(stringBuild.toString(), 1);
		 			}
		 			++count;
		 			stringBuild.delete(0, stringBuild.length());
		 		}
		 	}
		 	symbolInt = reader.read();
		}
		if(stringBuild.length() != 0)
 		{
 			if(hashMap.containsKey(stringBuild.toString()))
 			{
 				hashMap.put(stringBuild.toString(), hashMap.get(stringBuild.toString()) + 1);
 			}
 			else
 			{
 				hashMap.put(stringBuild.toString(), 1);
 			}
 			++count;
 			stringBuild.delete(0, stringBuild.length());
 		}
	}
}