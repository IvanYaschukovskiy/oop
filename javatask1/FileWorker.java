import java.lang.StringBuilder;
import java.io.*;
import java.util.*;

public class FileWorker
{
    public static void myWriter(String fileName, List<Map.Entry<String, Integer>> list, int count)
    {
        File file = new File(fileName);
        try 
        {
            if(!file.exists())
            {
                file.createNewFile();
            }
            FileWriter writer = new FileWriter(fileName + ".csv");
            try 
            {
                for(int i = 0; i < list.size(); ++i)
                {
                    writer.write(list.get(i).getKey() + "," + list.get(i).getValue() + "," + (double)list.get(i).getValue() / (double)count * 100.0 + "%" + "\n");
                }
            }
            finally 
            {
                writer.close();
            }
        }
        catch(IOException e)
        {
            e.printStackTrace(System.err);
        }
    }
}
