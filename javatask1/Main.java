import java.util.*;
import java.io.*;

public class Main {
	public static void main( String args[] )
	{
		if(args.length < 1)
		{
			System.err.println("Not enough command-line arguments");
			return;
		}
		Reader reader = null;
		MapAndCount mapAndCount = new MapAndCount();
        try
        {
            reader = new InputStreamReader(new FileInputStream(args[0]), "UTF-8");
            mapAndCount.doMap(reader);
        }
        catch (IOException e)
        {
            System.err.println("Error while reading file: " + e.getLocalizedMessage());
        }
        finally
        {
            if (null != reader)
            {
                try
                {
                    reader.close();
                }
                catch (IOException e)
                {
                    e.printStackTrace(System.err);
                }
            }
        }
        List<Map.Entry<String, Integer>> list = new ArrayList<Map.Entry<String, Integer>>(mapAndCount.hashMap.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() 
        {
        	public int compare(Map.Entry<String, Integer> map1, Map.Entry<String, Integer> map2) 
        	{
                return map2.getValue() - map1.getValue();
            }
        });
        FileWorker.myWriter(args[0], list, mapAndCount.count);
	}
}
