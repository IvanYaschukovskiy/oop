import Calculator.Calculator;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Scanner;

/**
 * Created by wanes on 17.03.2017.
 */
public class Main {
    public static void main(String[] args)
    {
        Calculator calc = new Calculator();
        try {
            calc.run(System.in, System.out);
        }
        catch (IOException e){
            System.out.println("Wrong configuration file " + e.getLocalizedMessage());
        }
    }
}
