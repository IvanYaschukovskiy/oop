package Calculator.Command;

import org.junit.Assert;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by wanes on 14.04.2017.
 */
public class PushTest {
    @Test
    public void execute() throws Exception {
        Context t = new Context();
        List<String> commandString = new LinkedList<String>();
        commandString.add("9.0");
        Push push = new Push();
        push.execute(commandString, t, System.out);
        Assert.assertEquals(9.0, t.peekAtStack(), 0);
    }

}