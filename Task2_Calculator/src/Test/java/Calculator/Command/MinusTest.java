package Calculator.Command;

import org.junit.Assert;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by wanes on 14.04.2017.
 */
public class MinusTest {
    @Test
    public void execute() throws Exception {
        Context t = new Context();
        List<String> commandString = new LinkedList<String>();
        t.pushToStack(15.0);
        t.pushToStack(10.0);
        Minus minus = new Minus();
        minus.execute(commandString, t, System.out);
        Assert.assertEquals(5, t.peekAtStack(), 0);
    }

}