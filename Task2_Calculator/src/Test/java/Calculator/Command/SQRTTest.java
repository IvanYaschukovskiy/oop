package Calculator.Command;

import org.junit.Assert;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by wanes on 14.04.2017.
 */
public class SQRTTest {
    @Test
    public void execute() throws Exception {
        Context t = new Context();
        List<String> commandString = new LinkedList<String>();
        t.pushToStack(9);
        SQRT sqrt = new SQRT();
        sqrt.execute(commandString, t, System.out);
        Assert.assertEquals(3.0, t.peekAtStack(), 0);
    }

}