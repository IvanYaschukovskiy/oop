package Calculator.Command;

import org.junit.Assert;
import org.junit.Test;

import java.io.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import static org.junit.Assert.*;

/**
 * Created by wanes on 14.04.2017.
 */
public class PrintTest {
    @Test
    public void execute() throws Exception {
        Context t = new Context();
        List<String> commandString = new LinkedList<String>();
        t.pushToStack(3);
        Print print = new Print();
        ByteArrayOutputStream byt1 = new ByteArrayOutputStream();
        PrintStream prt1 = new PrintStream(byt1);
        ByteArrayOutputStream byt2 = new ByteArrayOutputStream();
        PrintStream prt2 = new PrintStream(byt2);
        print.execute(commandString, t, prt2);
        prt1.println("3.0");
        Assert.assertArrayEquals(byt1.toByteArray(), byt2.toByteArray());
    }
}