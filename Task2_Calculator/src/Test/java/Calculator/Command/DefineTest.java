package Calculator.Command;

import org.junit.Assert;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by wanes on 14.04.2017.
 */
public class DefineTest {
    @Test
    public void execute() throws Exception {
        Context t = new Context();
        List<String> commandString1 = new LinkedList<String>();
        List<String> commandString2 = new LinkedList<String>();
        Define def = new Define();
        Push push = new Push();
        commandString1.add("a");
        commandString1.add("5.0");
        commandString2.add("a");
        def.execute(commandString1, t, System.out);
        push.execute(commandString2, t, System.out);
        Assert.assertEquals(5.0, t.peekAtStack(), 0);
    }

}