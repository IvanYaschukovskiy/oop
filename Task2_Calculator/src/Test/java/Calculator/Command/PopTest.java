package Calculator.Command;

import org.junit.Assert;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by wanes on 14.04.2017.
 */
public class PopTest {
    @Test
    public void execute() throws Exception {
        Context t = new Context();
        List<String> commandString = new LinkedList<String>();
        t.pushToStack(3);
        Pop pop = new Pop();
        pop.execute(commandString, t, System.out);
        Assert.assertEquals(0, t.sizeStack(), 0);
    }

}