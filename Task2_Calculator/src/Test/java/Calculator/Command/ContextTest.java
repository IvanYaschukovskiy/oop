package Calculator.Command;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by wanes on 14.04.2017.
 */
public class ContextTest {
    @Test
    public void pushToStack() throws Exception {
        Context t = new Context();
        t.pushToStack(3);
        Assert.assertEquals(3.0, t.peekAtStack(), 0);
    }

    @Test
    public void popFromStack() throws Exception {
        Context t = new Context();
        t.pushToStack(3);
        t.popFromStack();
        Assert.assertEquals(0, t.sizeStack(), 0);
    }

    @Test
    public void peekAtStack() throws Exception {
        Context t = new Context();
        t.pushToStack(3);
        double number = t.peekAtStack();
        Assert.assertEquals(3.0, number, 0);
    }

    @Test
    public void sizeStack() throws Exception {
        Context t = new Context();
        t.pushToStack(3);
        Assert.assertEquals(1, t.sizeStack(), 0);
    }

    @Test
    public void setMap() throws Exception {
        Context t = new Context();
        t.setMap("a", 5.0);
        Double number = t.getMap("a");
        Assert.assertEquals(5.0, number, 0);
    }


}