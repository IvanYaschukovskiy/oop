package Calculator.Command;

import org.junit.Assert;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by wanes on 14.04.2017.
 */
public class MultiplicationTest {
    @Test
    public void execute() throws Exception {
        Context t = new Context();
        List<String> commandString = new LinkedList<String>();
        t.pushToStack(10);
        t.pushToStack(2);
        Multiplication mult = new Multiplication();
        mult.execute(commandString, t, System.out);
        Assert.assertEquals(20.0, t.peekAtStack(), 0);
    }

}