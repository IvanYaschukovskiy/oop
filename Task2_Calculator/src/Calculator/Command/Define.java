package Calculator.Command;

import Calculator.CommandException.DoubleFormatException;
import Calculator.CommandException.NotNumberArgumentsException;
import Calculator.CommandException.StringFormatException;

import java.io.OutputStream;
import java.io.PrintStream;
import java.util.List;

/**
 * Created by wanes on 17.03.2017.
 */
public class Define implements Command
{
    public void execute(List<String> commandArgs, Context context, PrintStream out)
            throws NotNumberArgumentsException, DoubleFormatException, StringFormatException
    {
        if(commandArgs.size() != 2){
            throw new NotNumberArgumentsException("Wrong arguments");
        }
        Double tmp = null;
        try {
            tmp = Double.parseDouble(commandArgs.get(0));
        }
        catch (NumberFormatException n){
            try {
                Double number = Double.parseDouble(commandArgs.get(1));
                context.setMap(commandArgs.get(0), number);
            }
            catch (NumberFormatException e){
                throw new DoubleFormatException("No Double");
            }
        }
        if(tmp != null) {
            throw new StringFormatException("No String first parameter");
        }
    }
}
