package Calculator.Command;

import Calculator.CommandException.CalculatorException;

import java.io.PrintStream;
import java.util.List;

/**
 * Created by wanes on 21.03.2017.
 */
public interface Command {
    void execute(List<String> commandString, Context context, PrintStream out) throws CalculatorException;
}
