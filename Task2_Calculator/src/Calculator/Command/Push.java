package Calculator.Command;

import Calculator.CommandException.DoubleFormatException;
import Calculator.CommandException.NoSuchElementMapException;
import Calculator.CommandException.NotNumberArgumentsException;

import java.io.OutputStream;
import java.io.PrintStream;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Created by wanes on 17.03.2017.
 */
public class Push implements Command
{
    public void execute(List<String> commandArgs, Context context, PrintStream out)
            throws NotNumberArgumentsException, DoubleFormatException, NoSuchElementMapException
    {
        Double number;
        if(commandArgs.size() != 1) {
            throw new NotNumberArgumentsException("Wrong arguments");
        }
        try {
            number = Double.parseDouble(commandArgs.get(0));
        }
        catch (NumberFormatException e)
        {
            try {
                number = context.getMap(commandArgs.get(0));
            }
            catch (NoSuchElementException n){
                throw new NoSuchElementMapException("No such element in map");
            }
        }
        context.pushToStack(number);
    }
}
