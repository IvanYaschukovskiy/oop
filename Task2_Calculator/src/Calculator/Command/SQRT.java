package Calculator.Command;

import Calculator.CommandException.EmptyMyStackException;
import Calculator.CommandException.NotNumberArgumentsException;

import java.io.OutputStream;
import java.io.PrintStream;
import java.util.EmptyStackException;
import java.util.List;

import static java.lang.StrictMath.sqrt;

/**
 * Created by wanes on 17.03.2017.
 */
public class SQRT implements Command
{
    public void execute(List<String> commandString, Context context, PrintStream out)
            throws NotNumberArgumentsException, EmptyMyStackException
    {
        if(commandString.size() != 0){
            throw new NotNumberArgumentsException("Wrong arguments");
        }
        if(context.sizeStack() == 0) {
            throw new EmptyMyStackException("Empty stack");
        }
        try {
            Double number = context.popFromStack();
            context.pushToStack(sqrt(number));
        }
        catch (EmptyStackException s){
            throw new EmptyMyStackException("Empty stack");
        }
    }
}
