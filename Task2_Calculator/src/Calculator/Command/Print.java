package Calculator.Command;

import Calculator.CommandException.EmptyMyStackException;
import Calculator.CommandException.NotNumberArgumentsException;

import java.io.OutputStream;
import java.io.PrintStream;
import java.util.EmptyStackException;
import java.util.List;

/**
 * Created by wanes on 17.03.2017.
 */
public class Print implements Command
{
    public void execute(List<String> commandString, Context context, PrintStream out)
            throws NotNumberArgumentsException, EmptyMyStackException
    {
        if(commandString.size() != 0){
            throw new NotNumberArgumentsException("Wrong argument");
        }
        if(context.sizeStack() == 0) {
            throw new EmptyMyStackException("Empty stack");
        }
        try {
            out.println(context.peekAtStack());
        }
        catch (EmptyStackException s){
            throw new EmptyMyStackException("Empty stack");
        }
    }
}
