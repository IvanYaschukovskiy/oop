package Calculator.Command;

import Calculator.CommandException.NewInstanceException;
import Calculator.CommandException.NoSuchElementPropertiesException;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.NoSuchElementException;
import java.util.Properties;

/**
 * Created by wanes on 21.03.2017.
 */
public class Factory {
    private static Factory ourInstance = null;
    private static HashMap<String, Class<Command>> classes = null;
    private static Properties properties = null;

    public static Factory getInstance() throws IOException {
        if(ourInstance == null) {
            ourInstance = new Factory();
        }
        return ourInstance;
    }

    private Factory() throws IOException {
        classes = new HashMap<String, Class<Command>>();
        InputStream stream = Factory.class.getResourceAsStream("/Calculator/Factory.properties");
        properties = new Properties();
        properties.load(stream);
        stream.close();
    }

    public Command getCommand(String commandName)
            throws NoSuchElementPropertiesException, NewInstanceException
    {
        Command command = null;
        Class<?> cls = null;
        String key = properties.getProperty(commandName);
        if (key == null) {
            throw new NoSuchElementPropertiesException("No such command");
        }
        try {
            if (!classes.containsKey(key)) {
                cls = Class.forName(key);
                classes.put(commandName, (Class<Command>) cls);
            }
            else {
                cls = classes.get(commandName);
            }
            command = (Command) cls.newInstance();
            return command;
        }
        catch (IllegalAccessError i){
            throw new NewInstanceException("Error instance");
        }
        catch (InstantiationException i){
            throw new NewInstanceException("Error instance");
        }
        catch (NoSuchElementException n){
            throw new NoSuchElementPropertiesException("No such command");
        }
        catch (Exception e) {
            System.out.println("Exception " + e.getLocalizedMessage());
        }
        return command;
    }
}
