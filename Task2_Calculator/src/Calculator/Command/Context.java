package Calculator.Command;

import java.util.*;

/**
 * Created by wanes on 17.03.2017.
 */
public class Context
{
    private Map<String,Double> map;
    private Stack<Double> stack;
    public Context(){
        stack = new Stack<Double>();
        map = new HashMap<String, Double>();
    }
    public void pushToStack(double value){
        stack.push(value);
    }
    public double popFromStack()throws EmptyStackException{
        return stack.pop();
    }
    public double peekAtStack() throws EmptyStackException {
        return stack.peek();
    }
    public int sizeStack() {
        return stack.size();
    }
    public void setMap(String key, Double value){
        map.put(key, value);
    }
    public double getMap(String value)throws NoSuchElementException{
        if(!map.containsKey(value)){
            throw new NoSuchElementException("No such element " + value);
        }
        return map.get(value);
    }

}
