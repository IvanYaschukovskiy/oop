package Calculator.Command;

import Calculator.CommandException.EmptyMyStackException;
import Calculator.CommandException.NotEnoughArgumentException;
import Calculator.CommandException.NotNumberArgumentsException;

import java.io.OutputStream;
import java.io.PrintStream;
import java.util.EmptyStackException;
import java.util.List;

/**
 * Created by wanes on 17.03.2017.
 */
public class Div implements Command
{
    public void execute(List<String> commandString, Context context, PrintStream out)
            throws NotNumberArgumentsException, EmptyMyStackException, NotEnoughArgumentException
    {
        if(commandString.size() != 0){
            throw new NotNumberArgumentsException("Wrong arguments");
        }
        if(context.sizeStack() == 0) {
            throw new EmptyMyStackException("Empty stack");
        }
        if(context.sizeStack() < 2){
            throw new NotEnoughArgumentException("Not Enough argument stack");
        }
        try {
            Double number2 = context.popFromStack();
            Double number1 = context.popFromStack();
            context.pushToStack(number1 / number2);
        }
        catch (EmptyStackException s){
            throw new EmptyMyStackException("Empty stack");
        }
    }
}
