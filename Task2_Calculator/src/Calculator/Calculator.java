package Calculator;

import Calculator.Command.Command;
import Calculator.Command.Context;
import Calculator.Command.Factory;
import Calculator.CommandException.CalculatorException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by wanes on 17.03.2017.
 */
public class Calculator {
    private static final Logger logger = LogManager.getLogger(Calculator.class);
    public void run(InputStream in, PrintStream out) throws IOException
    {
        logger.info("\n===============================" +
                "\nNew start of program at " + java.util.Calendar.getInstance().getTime().toString());
        Scanner scanner = new Scanner(in);
        String string;
        Context context = new Context();
        System.out.println("Enter command:");
        while (scanner.hasNextLine()) {
            string = scanner.nextLine();
            if(string.length()==0)
            {
                logger.info("End of stream was found.");
                return;
            }
            logger.info("Was read line: " + string);
            Scanner scanString = new Scanner(string);
            List<String> commandString = new LinkedList<String>();
            while (scanString.hasNext()) {
                commandString.add(scanString.next());
            }
            String commandName = commandString.get(0);
            commandString.remove(0);
            Command command;
            try {
                command = Factory.getInstance().getCommand(commandName);
                command.execute(commandString, context, out);
                logger.trace("Command has executed: " + commandName+"");
            }
            catch(CalculatorException calcError){
                logger.error("Exception: " + calcError.getLocalizedMessage());
            }
        }
    }
}
