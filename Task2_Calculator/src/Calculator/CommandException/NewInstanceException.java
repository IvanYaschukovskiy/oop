package Calculator.CommandException;

/**
 * Created by wanes on 06.04.2017.
 */
public class NewInstanceException extends CalculatorException {
    public NewInstanceException(String value){
        super(value);
    }
}
