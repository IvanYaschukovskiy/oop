package Calculator.CommandException;

/**
 * Created by wanes on 21.03.2017.
 */
public class PopException extends CalculatorException
{
    public PopException(String value){
        super(value);
    }
    public void printError()
    {
        System.err.println("Stack empty");
    }
}
