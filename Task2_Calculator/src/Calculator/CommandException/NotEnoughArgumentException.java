package Calculator.CommandException;

/**
 * Created by wanes on 07.04.2017.
 */
public class NotEnoughArgumentException extends CalculatorException {
    public NotEnoughArgumentException(String value){
        super(value);
    }
}
