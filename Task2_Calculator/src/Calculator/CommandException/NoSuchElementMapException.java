package Calculator.CommandException;

/**
 * Created by wanes on 06.04.2017.
 */
public class NoSuchElementMapException extends CalculatorException {
    public NoSuchElementMapException(String value){
        super(value);
    }
}
