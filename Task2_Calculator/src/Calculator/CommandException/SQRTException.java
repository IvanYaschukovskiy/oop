package Calculator.CommandException;

/**
 * Created by wanes on 21.03.2017.
 */
public class SQRTException extends CalculatorException
{
    public SQRTException(String value){
        super(value);
    }
}
