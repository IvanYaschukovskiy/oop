package Calculator.CommandException;

/**
 * Created by wanes on 06.04.2017.
 */
public class EmptyMyStackException extends CalculatorException {
    public EmptyMyStackException(String value){
        super(value);
    }
}
