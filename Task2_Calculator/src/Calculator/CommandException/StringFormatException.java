package Calculator.CommandException;

/**
 * Created by wanes on 06.04.2017.
 */
public class StringFormatException extends CalculatorException {
    public StringFormatException(String value){
        super(value);
    }
}
