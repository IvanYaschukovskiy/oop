package Calculator.CommandException;

/**
 * Created by wanes on 06.04.2017.
 */
public class NotNumberArgumentsException extends CalculatorException {
    public NotNumberArgumentsException(String value){
        super(value);
    }
}
