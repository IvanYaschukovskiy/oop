package Calculator.CommandException;

/**
 * Created by wanes on 21.03.2017.
 */
public class MinusException extends CalculatorException
{
    public MinusException(String value){
        super(value);
    }
}
