package Calculator.CommandException;

/**
 * Created by wanes on 21.03.2017.
 */
public class PushException extends CalculatorException
{
    public PushException(String value) {
        super(value);
    }
}
