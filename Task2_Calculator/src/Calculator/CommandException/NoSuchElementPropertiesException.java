package Calculator.CommandException;

/**
 * Created by wanes on 06.04.2017.
 */
public class NoSuchElementPropertiesException extends CalculatorException {
    public NoSuchElementPropertiesException(String value){
        super(value);
    }
}
