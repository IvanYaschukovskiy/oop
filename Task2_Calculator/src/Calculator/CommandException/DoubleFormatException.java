package Calculator.CommandException;

/**
 * Created by wanes on 06.04.2017.
 */
public class DoubleFormatException extends CalculatorException {
    public DoubleFormatException(String value){
        super(value);
    }
}
