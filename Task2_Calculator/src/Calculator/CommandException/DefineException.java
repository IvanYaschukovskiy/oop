package Calculator.CommandException;

/**
 * Created by wanes on 21.03.2017.
 */
public class DefineException extends CalculatorException
{
    public DefineException(String value){
        super(value);
    }
}
