package resources;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by wanes on 06.06.2017.
 */
public class ResourcesManager {
    private BufferedImage image = null;
    private Map<String, BufferedImage> imageMap = new HashMap<String, BufferedImage>();

    public BufferedImage getImage(String nameImage){
        if(nameImage.equals("")){
            return null;
        }
        if(imageMap.containsKey(nameImage)){
            return imageMap.get(nameImage);
        }
        else {
            try {
                image = ImageIO.read(getClass().getResource("/resourcesManager/" +nameImage + ".png"));
            }
            catch (IOException e){
                e.printStackTrace();
            }
            imageMap.put(nameImage, image);
            return image;
        }
    }
}
