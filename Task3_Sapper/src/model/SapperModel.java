package model;

import level.Level;

import java.util.Random;

/**
 * Created by wanes on 26.05.2017.
 */
public class SapperModel {
    private SapperCell[][] gameField;
    private Level level = null;
    private boolean firstOpen;

    public SapperModel(Level level){
        this.level = level;
        createNewGame(level);
        firstOpen = true;
    }
    public boolean cellExist(int x, int y) {
        return (x >= 0) && (x < level.getWidth()) && (y >= 0) && (y < level.getHeight());
    }
    private void countRanks(){
        final int dX[] = {-1, 0, 1, 1, 1, 0, -1, -1};
        final int dY[] = {1, 1, 1, 0, -1, -1 , -1, 0};
        for (int i = 0; i < level.getWidth(); ++i){
            for (int j = 0; j < level.getHeight(); ++j){
                if(gameField[i][j].isBomber()) {
                    for (int k = 0; k < 8; ++k){
                        if(cellExist(i + dX[k], j + dY[k]) && gameField[i + dX[k]][j + dY[k]].isEmpty()){
                            SapperEmptyCell currCell = (SapperEmptyCell)gameField[i + dX[k]][j + dY[k]];
                            currCell.increment();
                        }
                    }
                }
            }
        }
    }
    private void setEmptyCells(){
        for (int i = 0; i < level.getWidth(); ++i) {
            for (int j = 0; j < level.getHeight(); ++j){
                if(gameField[i][j] == null){
                    gameField[i][j] = new SapperEmptyCell();
                }
            }
        }
        countRanks();
    }
    private void setBomber(){
        Random rand = new Random();
        for (int i = 0; i < level.getNumBomber(); ++i){
            boolean flag = false;
            while (!flag) {
                int x = rand.nextInt(level.getWidth() - 1);
                int y = rand.nextInt(level.getHeight() - 1);
                if(gameField[x][y] == null){
                    gameField[x][y] = new SapperBomberCell();
                    flag = true;
                }
            }
        }
    }
    public void createNewGame(Level newLevel){
        level = newLevel;
        createNewGame();
    }
    public void createNewGame(){
        gameField = new SapperCell[level.getWidth()][level.getHeight()];
        setBomber();
        setEmptyCells();
        firstOpen = true;
    }
    public Level getLevel(){
        return level;
    }
    public int getRank(int x, int y) {
        return ((SapperEmptyCell)gameField[x][y]).getRank();
    }
    public boolean isEmpty(int x, int y){
        return gameField[x][y].isEmpty();
    }
    public boolean isBomber(int x, int y){
        return gameField[x][y].isBomber();
    }
    public boolean isOpen(int x, int y){
        return gameField[x][y].isOpen();
    }
    public boolean isMark(int x, int y){
        return gameField[x][y].isMark();
    }
    public boolean isFirstOpen(){
        return firstOpen;
    }
    public void setFirstOpenFalse(){
        firstOpen = false;
    }
    public void setFlag(int x, int y){
        gameField[x][y].setFlag();
    }
    public void deleteFlag(int x, int y){
        gameField[x][y].deleteFlag();
    }
    public int openCell(int x, int y){
        gameField[x][y].open();
        return getRank(x, y);
    }
    public void openBomber(){
        for (int i = 0; i < level.getWidth(); ++i){
            for (int j = 0; j < level.getHeight(); ++j){
                if(gameField[i][j].isBomber()){
                    gameField[i][j].open();
                }
            }
        }
    }
}
