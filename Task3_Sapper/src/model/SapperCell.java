package model;

/**
 * Created by wanes on 27.05.2017.
 */
public abstract class SapperCell {
    private boolean open = false;
    private boolean mark = false;

    public boolean isMark() {
        return mark;
    }
    public boolean isOpen(){
        return open;
    }
    public void open(){
        open = true;
    }
    public void setFlag(){ //у него не так boolean
        mark = true;
    }
    public void deleteFlag(){ //у него не так boolean
        mark = false;
    }
    public abstract boolean isBomber();
    public abstract boolean isEmpty();
}
