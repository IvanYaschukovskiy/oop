package model;

/**
 * Created by wanes on 27.05.2017.
 */
public class SapperEmptyCell extends SapperCell {
    private int rank = 0;

    @Override
    public boolean isBomber() {
        return false;
    }

    @Override
    public boolean isEmpty() {
        return true;
    }
    public int getRank(){
        return rank;
    }
//    public void setRank(int bomber){
//        rank = bomber;
//    }
    public void increment(){
        ++rank;
    }
}
