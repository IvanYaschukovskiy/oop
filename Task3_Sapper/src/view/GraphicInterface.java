package view;

import controller.Controller;
import level.AdvancedLevel;
import level.BeginnerLevel;
import level.IntermediateLevel;
import level.Level;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;


/**
 * Created by wanes on 26.05.2017.
 */
public class GraphicInterface extends JFrame {
    private Controller controller = null;
    private Level level = null;
    private final short cellSize = 30;
    private GraphicPanel panel;
    private JLabel timeLabel;
    private Timer timer;
    private int second;
    private GraphicInterface thisView = this;

    public GraphicInterface(Level newlevel){
        level = newlevel;
        second = 0;
        controller = new Controller(level);
        setBackground(Color.BLACK);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Sapper");
        try{
            setIconImage(ImageIO.read(getClass().
                    getResourceAsStream("/resourcesManager/icon.png")));
        }
        catch (IOException e){
            e.printStackTrace();
        }
        setBounds(50, 50, cellSize * level.getWidth() + 6,
                cellSize * level.getHeight() + 52);
        setResizable(false);
        Container container = getContentPane();
        container.setLayout(new GridLayout(1,1,2,2));
        panel = new GraphicPanel(controller, level);
        container.add(panel);
        panel.addMouseListener(mouse);
        timer();
        createMenu();
    }

    private MouseListener mouse = new MouseListener() {
        @Override
        public void mouseClicked(MouseEvent e) {
            int x = e.getX() / cellSize;
            int y = e.getY() / cellSize;
            if(e.getButton() == e.BUTTON1){
                if(controller.isFirstOpen()){
                    timer.restart();
                    if(controller.isBomber(x, y)){
                        controller.startNewGame();
                    }
                    controller.setFirstOpenFalse();
                }
                controller.touch(x, y);
                repaint();
                if(controller.isGameOver()) {
                    repaint();
                    timer.stop();
                    int result;
                    if (controller.isWin()) {
                        result = JOptionPane.showConfirmDialog(panel, "time: "
                                + Integer.toString(second) + "\nНачать новую игру?",
                                "Вы выиграли!", 2);
                    }
                    else {
                        result = JOptionPane.showConfirmDialog(panel,
                                "Начать новую игру?","Вы проиграли!", 2);
                    }
                    if (result == JOptionPane.OK_OPTION) {
                        startNewGame();
                    } else {
                        System.exit(0);
                    }
                }
            }
            if(e.getButton() == e.BUTTON3){
                if(controller.isMark(x, y)){
                    controller.deleteFlag(x, y);
                }
                else {
                    controller.setFlag(x, y);
                }
                repaint();
            }
        }

        @Override
        public void mousePressed(MouseEvent e) {

        }

        @Override
        public void mouseReleased(MouseEvent e) {

        }

        @Override
        public void mouseEntered(MouseEvent e) {

        }

        @Override
        public void mouseExited(MouseEvent e) {

        }
    };

    private void createMenu(){
        JMenuBar menuBar = new JMenuBar();
        JMenu gameMenu = new JMenu("Game");
        JMenu levels = new JMenu("Levels");
        menuBar.add(gameMenu);
        menuBar.add(levels);
        JToolBar.Separator sep = new JToolBar.Separator();
        menuBar.add(sep);
        menuBar.add(timeLabel);
        JMenuItem newGame = new JMenuItem("New game");
        newGame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                timer.stop();
                startNewGame();
                repaint();
            }
        });
        gameMenu.add(newGame);
        JMenuItem records = new JMenuItem("Records");
        records.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });
        gameMenu.add(records);
        JMenuItem beginner = new JMenuItem("Beginner");
        beginner.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                level = new BeginnerLevel();
                timer.stop();
                startNewGame(level);
                setSize(cellSize * level.getWidth() + 6,
                        cellSize * level.getHeight() + 52);
                repaint();
            }
        });
        levels.add(beginner);
        JMenuItem intermediate = new JMenuItem("Intermediate");
        intermediate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                level = new IntermediateLevel();
                timer.stop();
                startNewGame(level);
                setSize(cellSize * level.getWidth() + 6,
                        cellSize * level.getHeight() + 52);
                repaint();
            }
        });
        levels.add(intermediate);
        JMenuItem advancedLevel = new JMenuItem("Advanced");
        advancedLevel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                level = new AdvancedLevel();
                timer.stop();
                startNewGame(level);
                setSize(cellSize * level.getWidth() + 6,
                        cellSize * level.getHeight() + 52);
                repaint();
            }
        });
        levels.add(advancedLevel);

        setJMenuBar(menuBar);
    }
    private void timer(){
        timeLabel = new JLabel("0");
        timeLabel.setBackground(Color.BLACK);
        timer = new Timer(1000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ++second;
                timeLabel.setText(Integer.toString(second));
            }
        });
    }
    private void startNewGame(){
        controller.startNewGame();
        second = 0;
        timeLabel.setText("0");
        repaint();
    }
    private void startNewGame(Level newLevel){
        controller.startNewGame(newLevel);
        second = 0;
        timeLabel.setText("0");
        repaint();
    }
}
