package view;

import controller.Controller;
import level.Level;
import model.SapperModel;
import resources.ResourcesManager;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by wanes on 13.06.2017.
 */
public class GraphicPanel extends JPanel {
    private Controller controller = null;
    private ResourcesManager resourcesManager = new ResourcesManager();
    private final short cellSize = 30;
    public GraphicPanel(Controller newController, Level newLevel){
        setBackground(Color.BLACK);
        controller = newController;
    }
    @Override
    public void paint(Graphics graphics){
        BufferedImage image;
        for (int i = 0; i < controller.getLevel().getWidth(); ++i){
            for (int j = 0; j < controller.getLevel().getHeight(); ++j) {
                if(controller.isMark(i, j)){
                    graphics.drawImage(resourcesManager.getImage("flag"),
                            i * cellSize, j * cellSize, null );
                }
                else if(!controller.isOpen(i, j)){
                    graphics.drawImage(resourcesManager.getImage("empty"),
                            i * cellSize, j * cellSize, null );
                }
                else if(controller.isBomber(i, j)){
                    graphics.drawImage(resourcesManager.getImage("bomber"),
                            i * cellSize, j * cellSize, null );
                }
                else {
                    graphics.drawImage(resourcesManager.getImage(Integer.toString(controller.getRank(i, j))),
                            i * cellSize, j * cellSize, null );
                }
            }
        }
    }

}
