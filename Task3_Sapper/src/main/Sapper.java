package main;

import controller.Controller;
import level.BeginnerLevel;
import model.SapperModel;
import view.GraphicInterface;

/**
 * Created by wanes on 12.05.2017.
 */
public class Sapper {
    public static void main(String[] args) {
        GraphicInterface graphicInterface = new GraphicInterface(new BeginnerLevel());
        graphicInterface.setVisible(true);
    }
}
