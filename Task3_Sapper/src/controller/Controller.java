package controller;

import level.Level;
import model.SapperModel;

import java.util.LinkedList;

/**
 * Created by wanes on 10.05.2017.
 */
public class Controller {
    static private final int[] dX = { -1, 0, 1, 1, 1, 0, -1, -1 };
    static private final int[] dY = { 1, 1, 1, 0, -1, -1, -1, 0 };

    private int countOpenedCells = 0;
    private boolean win = false;
    private boolean lose = false;
    private boolean gameOver = false;

    private SapperModel model = null;

    public Controller (Level newLevel){
        model = new SapperModel(newLevel);
    }
    public boolean isWin(){
        return win;
    }
    public boolean isLose(){
        return lose;
    }
    public boolean isGameOver(){
        return gameOver;
    }
    private void setWin(){
        win = true;
        gameOver = true;
    }
    private void setLose(){
        lose = true;
        gameOver = true;
    }
    private void recoverInitialValue(){
        win = false;
        lose = false;
        gameOver = false;
        countOpenedCells = 0;
    }
    public boolean touch(int x, int y){
        if(!model.cellExist(x, y)){
            return true;
        }
        if(!model.isOpen(x, y)){
            openEnvironment(x, y);
        }
        return isGameOver();
    }
    private void openEnvironment(int x, int y){
        if(!model.cellExist(x, y) || model.isOpen(x, y) || model.isMark(x, y)){
            return;
        }
        if(model.isBomber(x, y)){
            model.openBomber(); //надо открыть все клетки с бомбами
            setLose();
            return;
        }
        openCell(x, y);
        LinkedList<Integer> listCells = new LinkedList<Integer>();
        if(model.getRank(x, y) == 0){
            listCells.addLast(new Integer(x * 100 + y));
        }
        while (!listCells.isEmpty()){
            x = listCells.removeFirst().intValue();
            y = x % 100;
            x /= 100;
            for(int i = 0; i < 8; ++i){
                if(!model.cellExist(x + dX[i], y + dY[i]) || model.isOpen(x + dX[i], y + dY[i])
                        || model.isMark(x + dX[i], y + dY[i])){
                    continue;
                }
                openCell(x + dX[i], y + dY[i]);
                if(model.getRank(x + dX[i], y + dY[i]) == 0){
                    listCells.addLast(new Integer((x + dX[i]) * 100 + y + dY[i]));
                }
            }
        }
    }
    private void openCell(int x, int y){
        ++countOpenedCells;
        if(!model.cellExist(x, y)){
            return;
        }
        model.openCell(x, y);
        if(model.isEmpty(x, y)){
            //условие завершения игры
            if(model.getLevel().getWidth() * model.getLevel().getHeight()
                    - model.getLevel().getNumBomber() == countOpenedCells) {
                setWin();
                return;
            }
        }
        else { //попал в бомбу
            setLose();
            return;
        }
    }
    public void setFlag(int x, int y) {
        if(model.cellExist(x, y)){
            model.setFlag(x, y);
        }
    }
    public void deleteFlag(int x, int y) {
        if(model.cellExist(x, y)){
            model.deleteFlag(x, y);
        }
    }
    public void startNewGame(){
        recoverInitialValue();
        model.createNewGame();
    }
    public void startNewGame(Level newLevel){
        recoverInitialValue();
        model.createNewGame(newLevel);
    }
    public Level getLevel(){
        return model.getLevel();
    }
    public boolean isOpen(int x, int y){
        return model.isOpen(x, y);
    }
    public boolean isMark(int x, int y){
        return model.isMark(x, y);
    }
    public boolean isBomber(int x, int y){
        return model.isBomber(x, y);
    }
    public int getRank(int x, int y) {
        return model.getRank(x, y);
    }
    public boolean isFirstOpen(){
        return model.isFirstOpen();
    }
    public void setFirstOpenFalse(){
        model.setFirstOpenFalse();
    }
}
