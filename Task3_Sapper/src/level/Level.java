package level;

/**
 * Created by wanes on 27.05.2017.
 */
public interface Level {
    public short getNumBomber();
    public short getWidth();
    public short getHeight();
}
