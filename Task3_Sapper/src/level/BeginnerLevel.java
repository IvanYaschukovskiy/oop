package level;

/**
 * Created by wanes on 27.05.2017.
 */
public class BeginnerLevel implements Level{
    private static final short NUM_BOMBER = 10;
    private static final short WIDTH = 9;
    private static final short HEIGHT = 9;

    @Override
    public short getNumBomber(){
        return NUM_BOMBER;
    }
    @Override
    public short getWidth(){
        return WIDTH;
    }
    @Override
    public short getHeight(){
        return HEIGHT;
    }
}
