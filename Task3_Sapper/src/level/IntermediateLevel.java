package level;

/**
 * Created by wanes on 14.06.2017.
 */
public class IntermediateLevel implements Level {
    private static final short NUM_BOMBER = 40;
    private static final short WIDTH = 16;
    private static final short HEIGHT = 16;

    @Override
    public short getNumBomber(){
        return NUM_BOMBER;
    }
    @Override
    public short getWidth(){
        return WIDTH;
    }
    @Override
    public short getHeight(){
        return HEIGHT;
    }
}
