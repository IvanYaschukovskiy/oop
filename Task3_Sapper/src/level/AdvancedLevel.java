package level;

/**
 * Created by wanes on 14.06.2017.
 */
public class AdvancedLevel implements Level {
    private static final short NUM_BOMBER = 99;
    private static final short WIDTH = 30;
    private static final short HEIGHT = 16;

    @Override
    public short getNumBomber(){
        return NUM_BOMBER;
    }
    @Override
    public short getWidth(){
        return WIDTH;
    }
    @Override
    public short getHeight(){
        return HEIGHT;
    }
}
