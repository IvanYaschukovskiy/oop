import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;

import static com.sun.xml.internal.messaging.saaj.packaging.mime.util.ASCIIUtility.getBytes;

/**
 * Created by wanes on 19.10.2017.
 */
public class Message {
    TypeMessage typeMessage;
    public Message(String uid, TypeMessage value){
        typeMessage = value;
        generateMessage(uid, null, null, null);
    }
    public Message(String uid, TypeMessage value, String name, String msg){
        typeMessage = value;
        generateMessage(uid, null, name, msg);
    }
    public Message(String uid, TypeMessage value, InetSocketAddress address){
        typeMessage = value;
        generateMessage(uid, address, null, null);
    }
    private byte[] generateMessage(String uid, InetSocketAddress address, String name, String msg){
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        try {
            stream.write(uid.getBytes("UTF-8"));
            stream.write(typeMessage.toString().getBytes("UTF-8"));
            if (this.typeMessage == TypeMessage.MSG) {
                stream.write(name.getBytes("UTF-8"));
                stream.write(msg.getBytes("UTF-8"));
            }
            if (this.typeMessage == TypeMessage.NEW_PARENT){
                stream.write(address.toString().getBytes("UTF-8"));
            }
        }
        catch (UnsupportedEncodingException e){
            e.printStackTrace();
        }
        catch (IOException e){
            e.printStackTrace();
        }
        return stream.toByteArray();
    }
}
