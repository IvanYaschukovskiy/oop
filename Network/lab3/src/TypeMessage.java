/**
 * Created by wanes on 21.10.2017.
 */
public enum TypeMessage {
    ACK,
    MSG,
    NEW_PARENT,
    NEW_CHILD,
    ROOT,
    DEAD_CHILD
}
