import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by wanes on 25.09.2017.
 */
public class Server {
    public static void main(String[] args) {
        if(args.length < 1) {
            System.err.println("Not enough command-line arguments");
            return;
        }
        int port = 0;
        try{
            port = Integer.parseInt(args[0]);
        }
        catch (NumberFormatException e){
            e.printStackTrace();
        }
        try(ServerSocket serverSocket = new ServerSocket(port)){
            while (true){
                Socket clientSocket = serverSocket.accept();
                MultiThreadServer threadServer = new MultiThreadServer(clientSocket);
                new Thread(threadServer).start();
            }
        }
        catch (IOException e){
            e.printStackTrace();
        }



    }
}
