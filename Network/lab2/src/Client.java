import java.io.*;
import java.net.Socket;
import java.net.SocketException;

/**
 * Created by wanes on 25.09.2017.
 */
public class Client {
    public static final int BUFFER_SIZE = 1024;
    public static void main(String[] args) {
        if(args.length < 3) {
            System.err.println("Not enough command-line arguments");
            return;
        }
        int port = 0;
        DataInputStream inputStream = null;
        try{
            port = Integer.parseInt(args[2]);
        }
        catch (NumberFormatException e){
            System.out.println("ERROR: incorrect port" + e.getMessage());
        }
        try(FileInputStream file = new FileInputStream(args[0]);
            Socket clientSocket = new Socket(args[1], port)) {
            byte[] buffer = new byte[BUFFER_SIZE];
            long sizeFile = file.getChannel().size();
            int bytesRead = 0;
            long bytesReadAll = 0;
            int lastSlash;
            if(args[0].contains("/"))
                lastSlash = args[0].lastIndexOf('/');
            else
                lastSlash = args[0].lastIndexOf('\\');
            String fileName = args[0].substring(lastSlash + 1);
            inputStream = new DataInputStream(clientSocket.getInputStream());
            DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
            outToServer.writeUTF(fileName);
            outToServer.writeLong(sizeFile);
            while(bytesReadAll < sizeFile && (bytesRead = file.read(buffer)) != -1){
                bytesReadAll += bytesRead;
                outToServer.write(buffer, 0, bytesRead);
            }
            String message = inputStream.readUTF();
            System.out.println(message);
            clientSocket.close();
            file.close();
        }
        catch (SocketException e){
            try{
                System.out.println(inputStream.readUTF());
            }
            catch (IOException ex){
            }
        }
        catch (IOException e){
            e.printStackTrace();
        }

    }
}
