import java.io.*;
import java.net.Socket;
import java.net.SocketTimeoutException;

/**
 * Created by wanes on 06.10.2017.
 */
public class MultiThreadServer implements Runnable {
    public static final int BUFFER_SIZE = 1024;
    private Socket clientSocket = null;
    MultiThreadServer(Socket client){
        this.clientSocket = client;
    }
    @Override
    public void run() {
        PrintStream writeFile = null;
        try (DataOutputStream outToClient = new DataOutputStream(clientSocket.getOutputStream());
        DataInputStream inFromClient = new DataInputStream(clientSocket.getInputStream())) {
            byte[] buffer = new byte[BUFFER_SIZE];
            String nameFile;
            long sizeFile = 0;
            long bytesRead;
            long bytesReceived = 0;
            long bytesReceivedInThreeSeconds = 0;
            nameFile = inFromClient.readUTF();
            System.out.println(nameFile);
            sizeFile = inFromClient.readLong();
            String path = new File("").getAbsolutePath();
            if (!nameFile.contains("/") && !nameFile.contains("\\"))
                path += "/src/uploads/";
            else throw new ExceptionNameFile();
            writeFile = new PrintStream(new File(path + nameFile));
            long timer = System.currentTimeMillis();
            int counter = 0;
            while (bytesReceived < sizeFile && (bytesRead = inFromClient.read(buffer)) != -1) {
                bytesReceived += bytesRead;
                bytesReceivedInThreeSeconds += bytesRead;
                writeFile.write(buffer);
                if (System.currentTimeMillis() - timer > 3000){
                    System.out.println("Bytes/Second: " + bytesReceivedInThreeSeconds / 3);
                    ++counter;
                    bytesReceivedInThreeSeconds = 0;
                    timer = System.currentTimeMillis();
                }
                bufferFlush(buffer, (byte) 0);
            }
            System.out.println("Average Speed: " + bytesReceived / (3 * counter));
            outToClient.writeUTF("SUCCESS!!!");
            inFromClient.close();
            outToClient.close();
            writeFile.close();
            clientSocket.close();
        }
        catch (ExceptionNameFile e){
            try{
                clientSocket.close();
            }
            catch (IOException ex){

            }
        }
        catch (IOException e){
            writeFile.close();
            try{
                clientSocket.close();
            }
            catch (IOException ex){

            }
            e.printStackTrace();
        }
    }
    public void bufferFlush(byte[] array, byte value){
        int length = array.length;
        if (length > 0)
            array[0] = value;
        for(int i = 1; i < length; ++i)
            System.arraycopy(array,0, array, i,
                    ((length - i) < i)? (length - i) : i);
    }
}
