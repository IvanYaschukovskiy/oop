import Calculator.Calculator;

import java.util.Scanner;

/**
 * Created by wanes on 17.03.2017.
 */
public class Main {
    public static void main(String[] args)
    {
        Calculator calc = new Calculator();
        calc.run(System.in, System.out);

    }
}
