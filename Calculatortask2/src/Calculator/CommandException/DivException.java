package Calculator.CommandException;

/**
 * Created by wanes on 21.03.2017.
 */
public class DivException extends CalculatorException
{
    public DivException(String value){
        super(value);
    }
}
