package Calculator.CommandException;

/**
 * Created by wanes on 30.03.2017.
 */
public class ContextException extends CalculatorException {
    public ContextException(String value){
        super(value);
    }
}
