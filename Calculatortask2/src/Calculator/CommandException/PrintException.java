package Calculator.CommandException;

/**
 * Created by wanes on 21.03.2017.
 */
public class PrintException extends CalculatorException
{
    public PrintException(String value){
        super(value);
    }
}
