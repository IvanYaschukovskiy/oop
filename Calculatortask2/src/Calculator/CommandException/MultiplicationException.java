package Calculator.CommandException;

/**
 * Created by wanes on 21.03.2017.
 */
public class MultiplicationException extends CalculatorException
{
    public MultiplicationException(String value){
        super(value);
    }
}
