package Calculator.CommandException;

/**
 * Created by wanes on 21.03.2017.
 */
public class PlusException extends CalculatorException
{
    public PlusException(String value){
        super(value);
    }
}
