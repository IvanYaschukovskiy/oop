package Calculator;

import Calculator.Command.Command;
import Calculator.Command.Context;
import Calculator.Command.Factory;
import Calculator.CommandException.CalculatorException;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by wanes on 17.03.2017.
 */
public class Calculator {
    public void run(InputStream in, OutputStream out)
    {
        Scanner scanner = new Scanner(in);
        String string;
        Context context = new Context();
        System.out.println("Enter command:");
        while (scanner.hasNextLine()) {
            string = scanner.nextLine();
            Scanner scanString = new Scanner(string);
            List<String> commandString = new LinkedList<String>();
            while (scanString.hasNext()) {
                commandString.add(scanString.next());
            }
            String commandName = commandString.get(0);
            commandString.remove(0);
            Command command;
            try {
                command = Factory.getInstance().getCommand(commandName);
                command.execute(commandString, context, out);
               //System.out.println(context.stack.pop());
                //context.pushToStack(5.0);
            }
            catch (CalculatorException calcException)
            {

            }
        }
    }
}
