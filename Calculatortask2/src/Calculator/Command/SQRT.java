package Calculator.Command;

import Calculator.CommandException.SQRTException;

import java.io.OutputStream;
import java.util.List;

import static java.lang.StrictMath.sqrt;

/**
 * Created by wanes on 17.03.2017.
 */
public class SQRT implements Command
{
    public void execute(List<String> commandString, Context context, OutputStream out) throws SQRTException
    {
        try {
            if(commandString.size() != 0){
                throw new SQRTException("Wrong arguments");
            }
            Double number = context.popFromStack();
            context.pushToStack(sqrt(number));
        }
        catch (SQRTException p){

        }
        catch (Exception e){

        }
    }
}
