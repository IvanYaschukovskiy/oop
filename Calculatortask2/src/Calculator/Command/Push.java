package Calculator.Command;

import Calculator.CommandException.PushException;

import java.io.OutputStream;
import java.util.List;

/**
 * Created by wanes on 17.03.2017.
 */
public class Push implements Command
{
    public void execute(List<String> commandArgs, Context context, OutputStream out) throws PushException
    {
        Double number = null;
        try {
            if(commandArgs.size() != 1)
            {
                throw new PushException("Wrong arguments");
            }
            number = Double.parseDouble(commandArgs.get(0));
        }
        catch (NumberFormatException e)
        {
            number = context.getMap(commandArgs.get(0));
        }
        catch (PushException p){

        }
        catch (Exception e){

        }
        context.pushToStack(number);
    }
}
