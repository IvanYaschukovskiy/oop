package Calculator.Command;

import Calculator.CommandException.MinusException;

import java.io.OutputStream;
import java.util.List;

/**
 * Created by wanes on 17.03.2017.
 */
public class Minus implements Command
{
    public void execute(List<String> commandString, Context context, OutputStream out) throws MinusException
    {
        try {
            if(commandString.size() != 0){
                throw new MinusException("Wrong arguments");
            }
            Double number2 = context.popFromStack();
            Double number1 = context.popFromStack();
            context.pushToStack(number1 - number2);
        }
        catch (MinusException p){

        }
        catch (Exception e){

        }
    }
}
