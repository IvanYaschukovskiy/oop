package Calculator.Command;

import Calculator.CommandException.PrintException;

import java.io.OutputStream;
import java.util.List;

/**
 * Created by wanes on 17.03.2017.
 */
public class Print implements Command
{
    public void execute(List<String> commandString, Context context, OutputStream out) throws PrintException
    {
        try {
            if(commandString.size() != 0){
                throw new PrintException("Wrong argument");
            }
            System.out.println(context.peekAtStack());
        }
        catch (PrintException p){

        }
        catch (Exception e){

        }
    }
}
