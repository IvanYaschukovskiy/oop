package Calculator.Command;

import Calculator.CommandException.ContextException;
import Calculator.CommandException.PopException;

import java.io.OutputStream;
import java.util.List;

/**
 * Created by iashc on 17.03.2017.
 */
public class Pop implements Command
{
    public void execute(List<String> commandArgs, Context context, OutputStream out) throws PopException
    {
        try {
            if(commandArgs.size() != 0)
            {
                throw new PopException("Wrong arguments");
            }
            context.popFromStack();
        }
        catch (PopException p){

        }
        catch (Exception e){

        }
    }
}
