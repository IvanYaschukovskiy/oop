package Calculator.Command;

import Calculator.CommandException.DivException;

import java.io.OutputStream;
import java.util.List;

/**
 * Created by wanes on 17.03.2017.
 */
public class Div implements Command
{
    public void execute(List<String> commandString, Context context, OutputStream out) throws DivException
    {
        try {
            if(commandString.size() != 0){
                throw new DivException("Wrong arguments");
            }
            Double number2 = context.popFromStack();
            Double number1 = context.popFromStack();
            context.pushToStack(number1 / number2);

        }
        catch (DivException p){

        }
        catch (Exception e){

        }
    }
}
