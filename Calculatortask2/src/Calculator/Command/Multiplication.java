package Calculator.Command;

import Calculator.CommandException.MultiplicationException;

import java.io.OutputStream;
import java.util.List;

/**
 * Created by wanes on 17.03.2017.
 */
public class Multiplication implements Command
{
    public void execute(List<String> commandString, Context context, OutputStream out) throws MultiplicationException
    {
        try {
            if(commandString.size() != 0){
                throw new MultiplicationException("Wrong arguments");
            }
            Double number1 = context.popFromStack();
            Double number2 = context.popFromStack();
            context.pushToStack(number1 * number2);
        }
        catch (MultiplicationException p){

        }
        catch (Exception e){

        }
    }
}
