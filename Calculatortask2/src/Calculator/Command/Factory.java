package Calculator.Command;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.NoSuchElementException;
import java.util.Properties;

/**
 * Created by wanes on 21.03.2017.
 */
public class Factory {
    private static Factory ourInstance = null;
    private static HashMap<String, Class<Command>> classes = null;
    private static Properties properties = null;

    public static Factory getInstance() {
        if(ourInstance == null) {
            ourInstance = new Factory();
        }
        return ourInstance;
    }

    private Factory() {
        classes = new HashMap<String, Class<Command>>();
        InputStream stream = Factory.class.getResourceAsStream("/Calculator/Factory.properties");
        try {
            properties = new Properties();
            properties.load(stream);
            stream.close();
        }
        catch (IOException e) {
            System.out.println("IOException " + e.getLocalizedMessage());
        }
    }

    public Command getCommand(String commandName)
    {
        Command command = null;
        Class<?> cls = null;
        String key = properties.getProperty(commandName);
        try {
            if (key == null) {
                throw new NoSuchElementException("No such command " + key);
            }
            if (!classes.containsKey(key)) {
                cls = Class.forName(key);
                classes.put(commandName, (Class<Command>) cls);
            }
            else {
                cls = classes.get(commandName);
            }
            command = (Command) cls.newInstance();
            return command;
        }
        catch (NoSuchElementException n){
            
        }
        catch (Exception e) {
            System.out.println("Exception " + e.getLocalizedMessage());
        }
        return command;
    }
}
