package Calculator.Command;

import Calculator.CommandException.DefineException;

import java.io.OutputStream;
import java.util.List;

/**
 * Created by wanes on 17.03.2017.
 */
public class Define implements Command
{
    public void execute(List<String> commandArgs, Context context, OutputStream out) throws DefineException
    {
        try {
            if(commandArgs.size() != 2){
                throw new DefineException("Wrong arguments");
            }
            Double number = Double.parseDouble(commandArgs.get(1));
            context.setMap(commandArgs.get(0), number);
        }
        catch (DefineException p){

        }
        catch (NumberFormatException e){

        }
        catch (Exception e){

        }
    }
}
