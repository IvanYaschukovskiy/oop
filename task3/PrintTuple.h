#ifndef _PRINT_TUPLE_
#define _PRINT_TUPLE_

template <typename Tuple>
std::ostream& PrintTuple(std::ostream &out, Tuple const &tupl, _ind<1>) 
{
	out << std::get<std::tuple_size<Tuple>::value - 1>(tupl);
	return out;
}

template <typename Tuple, size_t ind>
std::ostream& PrintTuple(std::ostream &out, Tuple const &tupl, _ind<ind>) 
{
	out << std::get<std::tuple_size<Tuple>::value - ind>(tupl) << ", ";
	return PrintTuple(out, tupl, _ind<ind - 1>());
}

template <typename ...Args>
std::ostream& operator<<(std::ostream &out, std::tuple<Args...> const& tupl)
{
	PrintTuple(out, tupl, _ind<sizeof...(Args)>());
	return out;
}

#endif