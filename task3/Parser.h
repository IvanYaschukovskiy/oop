#ifndef _PARSER_
#define _PARSER_

#include <iostream>
#include <fstream>
#include <tuple>
#include <string>
#include <sstream>
#include <iterator>

typedef signed int		sint;
typedef unsigned int	uint;
typedef signed char		schar;
typedef unsigned char	uchar;

template<size_t> struct _ind {};

namespace csv 
{
	const sint CSV_FILE_OPEN_ERROR = -1001;
	const sint CSV_NOT_ENOUGH_VALUES = -1002;
	const sint CSV_READ_ECXEPTION = -1003;

	class ReadException : public std::exception {};

	struct _seps 
	{
		char scr;
		char deler;
		char delim;
	};

	struct _rerr 
	{
		size_t col;
		size_t row;
		size_t width;
	};

	template <typename ... Args>
	class CSVParser 
	{
		std::istream *file;
		bool mode;
		size_t count;
		_seps seps;
		_rerr rErr;
		bool flag;

		struct Iter : public std::iterator<std::input_iterator_tag, size_t> 
		{
			size_t strNum;
			CSVParser *parent;

		public:
			typedef std::iterator<std::input_iterator_tag, size_t>	base;
			typedef typename base::value_type						value_type;

			Iter(CSVParser &parent, value_type num = 0) : parent(&parent), strNum(num) {};
			Iter(const Iter &other) : parent(other.parent), strNum(other.strNum) {};

			Iter& operator=(const Iter& other) 
			{
				if (this != &other) {
					strNum = other.strNum;
					parent = other.parent;
				}

				return *this;
			};

			Iter& operator++() 
			{
				strNum += 1;
				return *this;
			};

			Iter operator++(int) 
			{
				Iter back(*this);
				strNum += 1;
				return back;
			};

			std::tuple<Args...> operator*() 
			{
				std::tuple<Args...> res;
				std::string line;

				getline(*(parent->file), line, parent->seps.delim);
				parent->rErr.row++;

				parent->flag = !line.empty();

				fill_tuple(res, line, parent->seps, parent->rErr);

				return res;
			};

			bool operator==(const Iter& other) const 
			{
				return (strNum == other.strNum);
			};

			bool operator!=(const Iter& other) const 
			{
				return !(strNum == other.strNum);
			};
		};

		void countLines() 
		{
			size_t curr, lines = 0;
			uchar ch;

			curr = file->tellg();

			file->seekg(0, file->beg);
			while (!file->eof()) 
			{
				ch = file->get();

				if (ch == '\n' || file->eof())
					lines++;
			}

			count = lines;

			file->clear();
			file->seekg(curr, file->beg);
		}

	public:
		CSVParser(std::istream &file, bool mode = false, size_t count = 0) : file(&file), mode(mode), count(count), seps({ '"', ',', '\n' }), rErr({ 1, 0, sizeof...(Args) }) 
		{
			if (mode)
				countLines();
		};

		Iter begin() 
		{
			return Iter(*this);
		};

		Iter end() 
		{
			if (!mode) 
			{
				countLines();
				mode = true;
			}

			return Iter(*this, count);
		};

		bool got() 
		{
			return this->flag;
		}

		_rerr getErr() 
		{
			return this->rErr;
		}
	};
}

#endif