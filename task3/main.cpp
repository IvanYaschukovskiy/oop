#include "Parser.h"
#include "FillTuple.h"
#include "PrintTuple.h"
#include <locale>

using namespace std;

int main() {
	using namespace csv;

	setlocale(0, "rus");

	string csvFile = "data.csv";

	ifstream file(csvFile);
	if (!file.good()) {
		cout << "Error open file: " << "\"" << csvFile << "\"" << endl;
		return CSV_FILE_OPEN_ERROR;
	}

	CSVParser<int, string, string, int, float, float, float, string, string, float> parser(file, 0);


	try {
		for (tuple<int, string, string, int, float, float, float, string, string, float> rs : parser) {
			if (parser.got())
				cout << rs << endl;
		}
	}
	catch (ReadException) {
		cout << endl << "Column: " << parser.getErr().width - parser.getErr().col << endl << "Line: " << parser.getErr().row << endl;
		return CSV_READ_ECXEPTION;
	}
	catch (out_of_range) {
		cout << endl << "Line: " << parser.getErr().row << " | Not enough values." << endl;
		return CSV_NOT_ENOUGH_VALUES;
	}
	cout << endl << "Parsing is over." << endl;

	return 0;
}