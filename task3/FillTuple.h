#ifndef _FILL_TUPLE_
#define _FILL_TUPLE_

#include <typeinfo>
#include "Parser.h"

template<class Tuple, std::size_t IND>
struct tuple_filler 
{
	static void fill_tuple(Tuple tupl, std::string line, const csv::_seps &seps, csv::_rerr &rErr)
	{
		if (line.empty())
			return;

		std::string string_value;

		if (line[line.size() - 1] == seps.scr)
		{
			line.pop_back();

			string_value = line.substr(line.find_last_of(seps.scr) + 1, line.size() - line.find_last_of(seps.scr));

			line.erase(line.find_last_of(seps.scr), line.size() - line.find_last_of(seps.scr));

			while (line[line.size() - 1] == seps.scr)
			{
				std::string string_subs;
				line.pop_back();
				string_subs = line.substr(line.find_last_of(seps.scr) + 1, line.size() - line.find_last_of(seps.scr));

				string_subs.push_back(seps.scr);

				string_value = (string_subs += string_value);
				line.erase(line.find_last_of(seps.scr), line.size() - line.find_last_of(seps.scr));
			}
			line.pop_back();
		}
		else
		{
			string_value = line.substr(line.find_last_of(seps.deler) + 1, line.size() - line.find_last_of(seps.deler));

			line.erase(line.find_last_of(seps.deler), line.size() - line.find_last_of(seps.deler));
		}

		rErr.col += 1;
		tuple_filler<Tuple, IND - 1>::fill_tuple(tupl, line, seps, rErr);

		if (typeid(std::get<IND - 1>(tupl)) == typeid(std::string))
		{
			std::get<IND - 1>(tupl) = (decltype(std::get<IND - 1>(tupl)))string_value;
		}
		else
		{
			std::stringstream str(string_value);

			auto tmp = std::get<IND - 1>(tupl);
			str >> tmp;

			if (str.fail()) 
			{
				rErr.col = rErr.width - IND;
				throw csv::ReadException();
				return;
			}

			std::get<IND - 1>(tupl) = tmp;
		}
	}
};

template<class Tuple>
struct tuple_filler<Tuple, 1> 
{
	static void fill_tuple(Tuple tupl, std::string line, const csv::_seps &seps, csv::_rerr &rErr)
	{
		std::string str;

		if (line[line.size() - 1] == seps.scr)
		{
			line.pop_back();

			str = line.substr(line.find_last_of(seps.scr) + 1, line.size() - line.find_last_of(seps.scr));

			line.erase(line.find_last_of(seps.scr), line.size() - line.find_last_of(seps.scr));

			while (line[line.size() - 1] == seps.scr)
			{
				std::string string_subs;
				line.pop_back();
				string_subs = line.substr(line.find_last_of(seps.scr) + 1, line.size() - line.find_last_of(seps.scr));

				string_subs.push_back(seps.scr);

				str = string_subs += str;
				line.erase(line.find_last_of(seps.scr), line.size() - line.find_last_of(seps.scr));
			}
			line.pop_back();
		}
		else
		{
			str = line;
		}

		if (typeid(std::get<0>(tupl)) == typeid(std::string))
		{
			std::get<0>(tupl) = (decltype(std::get<0>(tupl)))str;
		}
		else
		{
			std::stringstream str(str);

			auto tmp = std::get<0>(tupl);

			str >> tmp;

			if (str.fail())
			{
				rErr.col = rErr.width - 1;
				throw csv::ReadException();
				return;
			}

			std::get<0>(tupl) = tmp;
		}

		rErr.col = 0;
	}

};

template<class... Args>
void fill_tuple(std::tuple<Args...> &t, std::string line, csv::_seps seps, csv::_rerr &rErr)
{
	tuple_filler<decltype(t), sizeof...(Args)>::fill_tuple(t, line, seps, rErr);
}

#endif