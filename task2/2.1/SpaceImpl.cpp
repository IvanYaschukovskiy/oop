#include "SpaceImpl.hpp"
#include <string>
#include <iostream>

uint SpaceImpl::move(const Point point)
{
	if (this->field[this->current.y][this->current.x] != 'F' && this->field[this->current.y][this->current.x] != 'S')
	{
		this->field[this->current.y][this->current.x] = '*';
	}
	this->current = point;
	return this->distanceToFinish(point);
}

std::istream& operator >> (std::istream& inStream, SpaceImpl& spaceImpl)
{
	int i = 0;
	std::string inString;
	while (!inStream.eof())
	{
		getline(inStream,inString);
		spaceImpl.field.push_back(inString);
		for (uint j = 0; j < inString.length(); ++j)
		{
			if (inString[j] == 'F')
			{
				spaceImpl.finish = { (int)j,(int)i };
			}
			if (inString[j] == 'S')
			{
				spaceImpl.start = { (int)j,(int)i };
				spaceImpl.current = spaceImpl.start;
			}
		}
		++i;
	}
	return inStream;
}
std::ofstream& operator <<(std::ofstream &outStream, const SpaceImpl& spaceImpl)
{
	for (uint i = 0; i < spaceImpl.field.size(); i++)
	{
		outStream << spaceImpl.field[i] << std::endl;
	}
	return outStream;
}
Point SpaceImpl::getCurrent() const
{
	return this->current;
}
Point SpaceImpl::getFinish() const
{
	return this->finish;
}