#ifndef _SURFACE_
#define _SURFACE_

#include <vector>
#include <tuple>
#include <string>

const int _OUTPUT_FILE_OPEN_ERROR_ = -100;
const int _INPUT_FILE_OPEN_ERROR_ = -101;
const int _UNKNOWN_TOPOLOGY_TYPE_ = -102;
const int _BADMOVE_EXCEPTION_ = -103;
const int _GT_ALLOWABLE_LIMIT_ = -104;
const int _HELP_CALLED_ = 101;

typedef unsigned int uint;

struct Point
{
	int x;
	int y;
};

class Surface
{
public:;
	virtual uint move(const Point point) = 0;
	virtual std::vector <std::tuple<Point, uint>> lookup() = 0;
};

struct Config
{
	bool help = false;
	std::string space = "space.txt";
	std::string out = "route.txt";
	std::string dict = "dictionary.txt";
	int limit = 1000;
	std::string topology = "planar";
};

class BadMove : public std::exception {};

#endif