#ifndef _PLANAR_
#define _PLANAR_

#include "SpaceImpl.hpp"
class Planar :
	public SpaceImpl
{
public:
	virtual std::vector <std::tuple<Point, uint>> lookup();
	virtual uint distanceToFinish(const Point fromPoint);
};

#endif