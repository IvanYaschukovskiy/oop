#ifndef _ROBOT_
#define _ROBOT_

#include "SpaceImpl.hpp"
class Robot 
{
	SpaceImpl *space;

public:
	Robot(SpaceImpl *sp) : space(sp) {};
	void research();
	Point getClosestPath(const std::vector<std::tuple<Point, uint>> tuple);
};

#endif