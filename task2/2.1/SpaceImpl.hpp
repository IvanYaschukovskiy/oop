#ifndef _SPACEIMPL_
#define _SPACEIMPL_

#include <cstdlib>
#include "Surface.hpp"
#include <fstream>

class SpaceImpl :
	public Surface
{
protected:
	Point start;
	Point finish;
	Point current;
	std::vector<std::string> field;

public:
	virtual uint distanceToFinish(const Point fromPoint)=0;
	virtual uint move(const Point point);
	Point getCurrent() const;
	Point getFinish() const;
	friend std::istream& operator >> (std::istream&, SpaceImpl&);
	friend std::ofstream& operator << (std::ofstream&, const SpaceImpl&);
};

std::istream& operator >> (std::istream&, SpaceImpl&);
std::ofstream& operator << (std::ofstream&, const SpaceImpl&);

#endif