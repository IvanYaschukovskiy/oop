#include "SurfaceFactory.hpp"

SpaceImpl* SurfaceFactory::getSpaceImpl(std::string name)
{
	if (name == "planar")
	{
		return new Planar;
	}
	if (name == "tor")
	{
		return new Tor;
	}
	if (name == "cylinder")
	{
		return new Cylinder;
	}
	return nullptr;
}