#ifndef _CMD_PARSER_
#define _CMD_PARSER_

#include <cstring>
#include <cstdlib>
#include <iostream>
#include "Surface.hpp"
#include <string>

Config readParameters(int argc, char** argv);
void callHelp();

#endif