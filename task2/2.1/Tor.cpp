#include "Tor.hpp"

uint Tor::distanceToFinish(const Point fromPoint)
{
	uint distance1 = abs(fromPoint.x - this->finish.x);
	uint distance2 = this->field[fromPoint.y].size() - distance1;
	uint minDistance;
	minDistance = distance2;
	if (distance1 < distance2)
	{
		minDistance = distance1;
	}
	distance1 = abs(fromPoint.y - this->finish.y);
	distance2 = this->field.size() - distance1;
	if (distance1 < distance2)
	{
		return distance1 + minDistance;
	}
	return distance2 + minDistance;
}
std::vector<std::tuple<Point, uint>> Tor::lookup()
{
	std::vector<std::tuple<Point, size_t>> lookedUp;
	int height = this->field.size();

	Point points[] = 
	{
		{ (this->current.x - 1) < 0 ? (int)this->field[this->current.y].size() - 1 : this->current.x - 1, this->current.y },
		{ this->current.x, (this->current.y - 1) < 0 ? height - 1 : this->current.y - 1 },
		{ (this->current.x + 1) % (int)this->field[this->current.y].size(), this->current.y },
		{ this->current.x, (this->current.y + 1) % height }
	};

	for (Point p : points) 
	{
		if (p.x >= 0 && p.y >= 0 && p.y < height && p.x < this->field[p.y].size())
			if (this->field[p.y][p.x] == '.' || this->field[p.y][p.x] == 'F') 
			{
				lookedUp.push_back(std::make_tuple(p, this->distanceToFinish(p)));
			}
	}

	return lookedUp;
}
