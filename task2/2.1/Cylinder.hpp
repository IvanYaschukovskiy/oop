#ifndef _CYLINDER_
#define _CYLINDER_

#include "SpaceImpl.hpp"
class Cylinder :
	public SpaceImpl
{
public:
	virtual std::vector <std::tuple<Point, uint>> lookup();
	virtual uint distanceToFinish(const Point fromPoint);
};

#endif