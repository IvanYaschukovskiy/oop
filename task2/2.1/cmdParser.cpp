#include "cmdParser.hpp"

Config readParameters(int argc, char** argv)
{
	Config result;

	if (argc == 1)
		result.help = true;

	for (int i = 1; i < argc; ++i)
	{
		if (!strcmp(argv[i], "-h") || !strcmp(argv[i], "--help"))
		{
			result.help = true;
			break;
		}

		if (!strcmp(argv[i], "-s") || !strcmp(argv[i], "--space"))
		{
			result.space = argv[++i];
			continue;
		}

		if (!strcmp(argv[i], "-o") || !strcmp(argv[i], "--out"))
		{
			result.out = argv[++i];
			continue;
		}

		if (!strcmp(argv[i], "-l") || !strcmp(argv[i], "--limit"))
		{
			result.limit = atoi(argv[++i]);
			continue;
		}

		if (!strcmp(argv[i], "-t") || !strcmp(argv[i], "--topology"))
		{
			result.topology = argv[++i];
			continue;
		}
	}

	return result;
}

void callHelp()
{
	std::cout << "(-h --help) - ������ ��������� �� ������� ���������� ��������� � ���������� ������." << std::endl;
	std::cout << "(-s --space) - ��� ���������� ����� � ��������� ������������. (space.txt �� ���������)" << std::endl;
	std::cout << "(-o --out) - �������� ��������� ���� � ���������. (route.txt �� ���������)" << std::endl;
	std::cout << "(-l --limit) - ����� ������������ ����� ���������. ���� ������� ��������� ������ ����� �� �������������. (1000 �� ���������)" << std::endl;
	std::cout << "(-t --topology) - ������������, ������� ���������� ��������� ������������. (planar �� ���������)" << std::endl;
	std::cout << "\tplanar - ������� ����" << std::endl;
	std::cout << "\tcylinder - ���� �������� ����������� �������, ��� ���������� ������� ��� ������ ���� ����� ����� ����������� �� ������ ����� �����" << std::endl;
	std::cout << "\ttor - ���� �������� ����������� ���, ������������� � �������� �������� ������� ��� ���������� ������� � ������ ������" << std::endl;
}