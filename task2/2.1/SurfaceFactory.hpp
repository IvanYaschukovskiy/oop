#ifndef _SURFACE_FACTORY_
#define _SURFACE_FACTORY_

#include "SpaceImpl.hpp"
#include "Cylinder.hpp"
#include "Tor.hpp"
#include "Planar.hpp"

class SurfaceFactory
{
	SurfaceFactory(){}

public:
	static SurfaceFactory & getInstance()
	{
		static SurfaceFactory f;
		return f;
	}
	SpaceImpl* getSpaceImpl(std::string name);
};

#endif