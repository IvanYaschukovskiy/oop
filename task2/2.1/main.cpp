#include <locale>

#include "Surface.hpp"
#include "SpaceImpl.hpp"
#include "Cylinder.hpp"
#include "Planar.hpp"
#include "Tor.hpp"
#include "cmdParser.hpp"
#include "Robot.hpp"
#include "SurfaceFactory.hpp"

int main(int argc, char** argv)
{
	setlocale(0, "Russian");
	Config parameters;
	parameters = readParameters(argc, argv);
	if (parameters.help)
	{
		callHelp();
		return _HELP_CALLED_;
	}
	std::ifstream from(parameters.space);
	if (!from.good())
	{
		std::cout << "Error open file: " << "\"" << parameters.space << "\"" << std::endl;
		return _INPUT_FILE_OPEN_ERROR_;
	}
	SpaceImpl* space = SurfaceFactory::getInstance().getSpaceImpl(parameters.topology);
	/*if (parameters.topology == "planar")
	{
		space = new Planar;
	}
	else
	{
		if (parameters.topology == "cylinder")
		{
			space = new Cylinder;
		}
		else
		{
			if (parameters.topology == "tor")
			{
				space = new Tor;
			}
			else
			{
				std::cout << "Unknown space: " << "\"" << parameters.topology << "\"" << std::endl;
				return _UNKNOWN_TOPOLOGY_TYPE_;
			}
		}
	}*/
	from >> *space;
	std::ofstream to(parameters.out);
	if (!to.good()) 
	{
		std::cout << "Error open file: " << "\"" << parameters.out << "\"" << std::endl;
		return _OUTPUT_FILE_OPEN_ERROR_;
	}
	Robot robot(space);
	try 
	{
		robot.research();
	}
	catch (...)
	{
		std::cout << "Path not found" << std::endl;
		to << "Path not found" << std::endl;
		delete space;
		return _BADMOVE_EXCEPTION_;
	}
	to << *space;
	delete space;
	return 0;
}