#include "Robot.hpp"
#include <iostream>

void Robot::research()
{
	std::tuple<Point, uint> next;
	Point pNext;
	uint distance;
	do {
		pNext = getClosestPath(space->lookup());
		distance = space->move(pNext);
	} while (distance);
}

Point Robot::getClosestPath(const std::vector<std::tuple<Point, uint>> tuple)
{
	if(tuple.size() == 0)
		throw BadMove();
	
	uint minDistance = std::get<1>(tuple[0]);
	Point closestInFinishPoint = std::get<0>(tuple[0]);
	for (uint i = 0; i < tuple.size(); ++i)
	{
		if (minDistance > std::get<1>(tuple[i]))
		{
			minDistance = std::get<1>(tuple[i]);
			closestInFinishPoint = std::get<0>(tuple[i]);
		}
	}
	return closestInFinishPoint;
}