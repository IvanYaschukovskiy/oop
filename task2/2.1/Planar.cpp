#include "Planar.hpp"
#include <iostream>

std::vector<std::tuple<Point, uint>> Planar::lookup()
{
	std::vector<std::tuple<Point, uint>> lookedUp;
	int a[4] = { -10, -1, 1, 10 };
	Point currentPoint = { this->current.x, this->current.y};
	int sizeX = this->field[currentPoint.y].size();
	int sizeY = this->field.size();
	for (uint i = 0; i<4; ++i)
	{
		if ((currentPoint.y + (a[i] / 10)<sizeY) && (currentPoint.x + (a[i] % 10)<sizeX) && (currentPoint.y + (a[i] / 10) >= 0) && (currentPoint.x + (a[i] % 10) >= 0))
		{
			if (this->field[currentPoint.y + (a[i] / 10)][currentPoint.x + (a[i] % 10)] == '.' || this->field[currentPoint.y + (a[i] / 10)][currentPoint.x + (a[i] % 10)] == 'F')
			{
				Point nextPoint = { currentPoint.x + (a[i] % 10),currentPoint.y + (a[i] / 10) };
				uint distance = this->distanceToFinish(nextPoint);
				lookedUp.push_back(std::make_tuple(nextPoint, distance));
			}
		}
	}
	return lookedUp;
}

uint Planar::distanceToFinish(const Point fromPoint)
{
	return abs(fromPoint.x - this->finish.x) + abs(fromPoint.y - this->finish.y);
}
