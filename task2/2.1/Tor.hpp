#ifndef _TOR_
#define _TOR_

#include "SpaceImpl.hpp"
class Tor :
	public SpaceImpl
{
public:
	virtual std::vector <std::tuple<Point, uint>> lookup();
	virtual uint distanceToFinish(const Point fromPoint);
};

#endif