#include "Surface.hpp"
#include "Config.hpp"

int main(int argc, char** argv) 
{

	setlocale(0, "Russian");

	Config parameters;
	uint dist = 0;
	uint ind = 0;
	std::ifstream from;
	std::ofstream to;

	parameters.readParameters(argc, argv);

	if (parameters.help) 
	{
		callHelp();
		return _HELP_CALLED_;
	}

	from.open(parameters.space);
	if (!from.good()) 
	{
		std::cout << "Error open file: " << "\"" << parameters.space << "\"" << std::endl;
		return _INPUT_FILE_OPEN_ERROR_;
	}

	to.open(parameters.out);
	if (!to.good()) 
	{
		std::cout << "Error open file: " << "\"" << parameters.out << "\"" << std::endl;
		return _OUTPUT_FILE_OPEN_ERROR_;
	}

	try 
	{
		if (parameters.topology == "planar") 
		{
			Planar space(from);
			Robot<Point, uint> robot(&space);
			robot.research();
			robot.showPath(to);
		}
		else if (parameters.topology == "dict") 
		{
			Dictionary space(from, parameters.dict);
			Robot<std::string, uint> robot(&space);
			robot.research();
			robot.showPath(to);
		}
	}
	catch (FileError) 
	{
		std::cout << "Couldn't open dictionary: " << "\"" << parameters.dict << "\"" << std::endl;
		return _DICTIONARY_FILE_OPEN_ERROR;
	}
	catch (BadMove) 
	{
		std::cout << "Path not found" << std::endl;
		return _BADMOVE_EXCEPTION_;
	}


	return 0;
}