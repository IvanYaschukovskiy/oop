#ifndef _CONFIG_
#define _CONFIG_

#include "Surface.hpp"

struct Config {
	bool help = false;
	std::string space = "space.txt";
	std::string out = "route.txt";
	std::string dict = "dictionary.txt";
	int limit = 1000;
	std::string topology = "planar";
	readParameters(int argc, char** argv);
};
void callHelp();

#endif