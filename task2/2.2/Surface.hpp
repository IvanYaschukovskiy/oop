#ifndef _SURFACE_
#define _SURFACE_

#include <iostream>
#include <vector>
#include <tuple>
#include <fstream>
#include <string>
#include <limits>
#include <ctime>
#include <cmath>
#include <cstdlib>
#include <cstring>

const int _OUTPUT_FILE_OPEN_ERROR_ = -100;
const int _INPUT_FILE_OPEN_ERROR_ = -101;
const int _UNKNOWN_TOPOLOGY_TYPE_ = -102;
const int _BADMOVE_EXCEPTION_ = -103;
const int _GT_ALLOWABLE_LIMIT_ = -104;
const int _DICTIONARY_FILE_OPEN_ERROR = -105;

const int _HELP_CALLED_ = 101;

typedef unsigned int uint;
typedef unsigned char uchar;

struct Point {
	int x;
	int y;
};

class BadMove : public std::exception {};
class FileError : public std::exception {};
 
template <class P, class M>
class Surface
{
public:
	virtual M distance(P &from, P &to) = 0;
	virtual std::tuple<P, M> getClosest(std::vector<std::tuple<P, M>> &lookupResults) = 0;
	//M research();

	virtual M move(P &point) throw (BadMove) = 0;
	virtual std::vector<std::tuple<P, M>> lookup() = 0;
};

class Planar: public Surface <Point, uint>
{
public:
	Point start, finish, curr;
	std::vector<std::vector<uchar>> matrix;

	Planar(std::istream &from);
	~Planar();
	virtual uint distance(Point &from, Point &to);
	virtual std::tuple<Point, uint> getClosest(std::vector<std::tuple<Point, uint>> &lookupResults);

	virtual uint move(Point &point) throw (BadMove);
	virtual std::vector<std::tuple<Point, uint>> lookup();
};

class Dictionary: public Surface <std::string, uint> {
public:
	std::string start, finish, curr, dict;
	std::vector<std::vector<std::string>> cache;
	std::vector<std::string> path;
	bool isRead;

	Dictionary(std::istream &from, std::string dictFile);
	~Dictionary();
	virtual uint distance(std::string &from, std::string &to);
	virtual std::tuple<std::string, uint> getClosest(std::vector<std::tuple<std::string, uint>> &lookupResults);

	virtual uint move(std::string &word) throw (BadMove);
	virtual std::vector<std::tuple<std::string, uint>> lookup() throw (FileError);
};

template<class P, class M>
class Robot {
	Surface<P, M> *space;

public:
	Robot(Surface<P, M> *space) : space(space) {};

	void research() 
	{
		std::tuple<P, M> next;
		P wNext;
		do 
		{
			next = space->getClosest(space->lookup());
			wNext = std::get<0>(next);
		} while (space->move(wNext));
	}
	void showPath(std::ostream &to) 
	{
		to << *(this->space);
	}
};

std::ostream& operator<<(std::ostream &to, Planar &space);
std::istream& operator >> (std::istream &from, Planar &space);

std::ostream& operator<<(std::ostream &to, Dictionary &space);
std::istream& operator >> (std::istream &from, Dictionary &space);

#endif