#include <iostream>
#include <fstream>
#include <string>
#include <list>
#include <iterator>
#define N 40
using namespace std;
int main(int argc, char** argv)
{
	if (argc > 3)
	{
		cout << "An input command line argument";
		return -1;
	}
	ifstream myFileIn(argv[1]);
	if (!myFileIn)
	{
		cout << "Error open file\n";
		return -1;
	}
	char buffer[N];
	list<string> myList;
	while (myFileIn.getline(buffer, N))
	{
		myList.push_back(buffer);
	}
	myList.sort();
	ofstream myFileOut(argv[2]);
	if (!myFileOut)
	{
		cout << "Error open file\n";
		return -1;
	}
	copy(myList.begin(), myList.end(), ostream_iterator<string>(myFileOut, "\n"));
	return 0;
}